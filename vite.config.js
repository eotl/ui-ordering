import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import { viteStaticCopy } from 'vite-plugin-static-copy'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        viteStaticCopy({
            targets: [
                // Not sure if this is the best solution, but the files in /dist are not served in development so you can't read them. But now you don't see a difference between the coppied files and the local ones
                // Maybe we need to make an extra folder in like `external` or something that shows these files are being coppied there...
                { src: path.resolve(__dirname, 'node_modules/@eotl/icons/dist/*'), dest: 'images/icons' },
                {
                    src: path.resolve(__dirname, 'node_modules/@eotl/theme-bootstrap/dist/fonts/*'),
                    dest: 'fonts',
                },
                { src: path.resolve(__dirname, 'node_modules/@eotl/theme-bootstrap/dist/js/*'), dest: 'js' },
                { src: path.resolve(__dirname, 'node_modules/@eotl/theme-bootstrap/dist/css/*'), dest: 'css' },
            ],
        }),
        vue(),
    ],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src'),
        },
        extensions: ['.js', '.vue'],
    },
    build: {
        lib: {
            entry: path.resolve(__dirname, 'src/index.js'),
            name: 'ui-ordering',
        },
        rollupOptions: {
            external: ['vue', 'pinia', 'jquery']
        }
    },
    server: {
        port: 8080,
        host: '0.0.0.0',
        build: {
            sourcemap: true,
            chunkSizeWarningLimit: 25000,
            rollupOptions: {
                output: {
                    manualChunks: {
                        libs: ['vue'],
                    },
                },
            },
        },
    },
})
