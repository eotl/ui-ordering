import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import { viteStaticCopy } from 'vite-plugin-static-copy'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        viteStaticCopy({
            targets: [
                { src: path.resolve(__dirname, 'node_modules/@eotl/icons/dist/*'), dest: 'images/icons' },
                { src: path.resolve(__dirname, 'node_modules/@eotl/theme-bootstrap/dist/fonts/*'), dest: 'fonts' },
                { src: path.resolve(__dirname, 'node_modules/@eotl/theme-bootstrap/dist/js/*'), dest: 'js' },
                { src: path.resolve(__dirname, 'node_modules/@eotl/theme-bootstrap/dist/css/*'), dest: 'css' },
            ],
        }),
        vue(),
    ],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src'),
        },
        extensions: ['.js', '.vue'],
    },
    build: {
        outDir: './dist',
        emptyOutDir: false,
        lib: {
            formats: ['es'],
            entry: path.resolve(__dirname, 'src/index.js'),
            fileName: 'ui-ordering-module',
        },
        rollupOptions: {
            external: [/^@eotl\/core/, 'vue', 'pinia', 'jquery'],
        },
    },
})
