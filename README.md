# UI Ordering

## Configuation

```
<div id="app"></div>
<script>
    const ordering = {
        dispatchUrl: 'https://dispatch.eotl.supply',
        inventoryUrl: 'https://inventory.eotl.supply',
        coordinator: 'ecologistik',
        api: '/api/',
        lang: 'en',
        currency: '€',
        style: 'theme-white',
        iconPath: '/icons/',
        imagePath: '/items/theme/',
        layout: 'list',
        checkoutHeader: true,
        checkoutType: 'priced'
    }
</script>
<script type="text/javascript" src="js/libs.js"></script>
<script type="text/javascript" src="js/ui-ordering.js"></script>
<script type="text/javascript" src="js/app.js"></script>
```

**coordinator**

-   `coordinator-id` - an Open-Dispatch `username` or `identityKey`

**api**

-   `http://open-inventory.server.com` - a URL or path to the JSON of inventory

**layout**

-   `list` - shows list of items grouped by category (small # of items)
-   `pages` - shows category page then pages of items (large # of items)

## Development

### Via Docker & Makefile

Install depedencies from NPM

```
make packages
```

Build the app

```
make build
```

Serve it locally

```
yarn serve
```


For more commands, check `package.json -> scripts`. Each command can be run as `yarn commandName`, for example: `yarn lint`.

To open a shell within the container, where to run commands, like `yarn` for example, from the Docker host machine, you can run `make shell`, and a bash shell will be provided.


See [Configuration Reference](https://cli.vuejs.org/config/) for customization
