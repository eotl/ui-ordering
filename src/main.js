import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import i18n from './i18n'
import { createPinia } from 'pinia'
import { useEotlCore } from '@eotl/core/store'

if (window.orderingConfig == undefined) {
    window.orderingConfig = {
        dispatchUrl: 'http://localhost:8080',
        inventoryUrl: 'http://localhost:8000',
        coordinator: 'testing',
        api: '/fixtures/',
        api_ending: '.json',
        lang: 'en',
        currency: '€',
        iconPath: '/icons/',
        imagePath: '/items/simple/',
        layout: 'list',
        checkoutHeader: true,
        checkoutType: 'priced',
    }
}

const app = createApp(App)
const pinia = createPinia()

app.use(router).use(pinia).use(i18n).mount('#app')

useEotlCore().init({
    ...window.orderingConfig,
    url_api: window.orderingConfig.api
})

// TODO??
// Vue.config.productionTip = false;
