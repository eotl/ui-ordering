import { mount } from '@vue/test-utils'
import { createPinia } from 'pinia'
import { describe, expect, beforeEach, vi, test } from 'vitest'
import flushPromises from 'flush-promises'
import Item from '@/components/Item.vue'
import { useStore } from '@/store' // Import useStore
import i18n from '@/i18n/index'
import router from '@/router/index'

// Mock store
const mockStore = {
    iconPath: '/icons/',
    checkoutHeader: true,
    config: {
        checkoutHeader: true,
        iconPath: '/icons/',
        layout: 'pages',
    },
    order: [{ cost: 10 }, { cost: 20 }],
    items: [{ id: 1, name: 'Item 1', categoryId: 'test-category' }, { id: 2, name: 'Item 2', categoryId: 'test-category' }],
    categories: [{ id: 1, name: 'Category 1' }, { id: 2, name: 'Category 2' }],
    fetchItems: vi.fn().mockResolvedValue([]),
    fetchCategories: vi.fn().mockResolvedValue([]),
    alertWarning: vi.fn(),
    addItemToCart: vi.fn(),
    removeItemFromCart: vi.fn(),
}

vi.mock('@/store', () => ({
    useStore: vi.fn(() => mockStore),
}))

vi.mock('@eotl/core/store', () => ({
    useEotlCore: vi.fn(() => ({
        fetchGet: vi.fn((url) => {
            if (url === '/categories') {
                return Promise.resolve({
                    status: 'success',
                    data: mockStore.categories,
                })
            }
            return Promise.resolve({ status: 'error', error: { value: 'Not found' } })
        }),
        alertWarning: vi.fn(),
    })),
}))


describe('Item.vue', () => {
    let pinia

    beforeEach(async () => {
        pinia = createPinia()
        router.push('/')
        await router.isReady()
    })

    test('Renders Items correctly', async () => {
        router.push({ path: '/', query: { cat: 'test-category' } })
        await router.isReady()

        const wrapper = mount(Item, {
            global: {
                plugins: [pinia, i18n, router],
                stubs: {
                    'router-link': true,
                    'router-view': true,
                },
                mocks: {
                    $route: {
                        query: {
                            cat: 'test-category',
                        },
                    },
                },
            },
            props: {
                itemId: 'test-item',
                data: { 
                    item: mockStore.items[0], 
                    details: [], 
                    inventoryItems: [] 
                }, 
                itemSize: 'medium', 
                currency: 'USD', 
            },
        })

        await flushPromises()
        await wrapper.vm.$nextTick()

        expect(wrapper.findComponent(Item).exists()).toBe(true)
    })

    test('Properly adds Item to cart', async () => {
        router.push({ path: '/', query: { cat: 'test-category' } })
        await router.isReady()

        const store = useStore()
        const wrapper = mount(Item, {
            global: {
                plugins: [pinia, i18n, router],
                stubs: {
                    'router-link': true,
                    'router-view': true,
                },
                mocks: {
                    $route: {
                        query: {
                            cat: 'test-category',
                        },
                    },
                },
            },
            props: {
                itemId: 'test-item',
                data: { 
                    item: mockStore.items[0], 
                    details: [], 
                    inventoryItems: [] 
                }, 
                itemSize: 'medium',
                currency: 'USD', 
            },
        })

        await flushPromises()
        await wrapper.vm.$nextTick()

        store.addItemToCart({ id: 'test-item', cost: 10 })
        mockStore.order.push({ id: 'test-item', cost: 10 }) // Update the mock store
 
        expect(store.addItemToCart).toHaveBeenCalledWith({ id: 'test-item', cost: 10 })
        expect(store.order.length).toBe(3)
    })

    test('Properly subtracts Item from cart', async () => {
        router.push({ path: '/', query: { cat: 'test-category' } })
        await router.isReady()

        const store = useStore()
        const wrapper = mount(Item, {
            global: {
                plugins: [pinia, i18n, router],
                stubs: {
                    'router-link': true,
                    'router-view': true,
                },
                mocks: {
                    $route: {
                        query: {
                            cat: 'test-category',
                        },
                    },
                },
            },
            props: {
                itemId: 'test-item',
                data: { 
                    item: mockStore.items[0], 
                    details: [], 
                    inventoryItems: [] 
                }, 
                itemSize: 'medium',
                currency: 'USD', 
            },
        })

        await flushPromises()
        await wrapper.vm.$nextTick()

        store.removeItemFromCart('test-item')
        mockStore.order = mockStore.order.filter(order => order.id !== 'test-item') 

        expect(store.removeItemFromCart).toHaveBeenCalledWith('test-item')
    })

    test('Total in Cart is correct', async () => {
        router.push({ path: '/', query: { cat: 'test-category' } })
        await router.isReady()

        const store = useStore()
        const wrapper = mount(Item, {
            global: {
                plugins: [pinia, i18n, router],
                stubs: {
                    'router-link': true,
                    'router-view': true,
                },
                mocks: {
                    $route: {
                        query: {
                            cat: 'test-category',
                        },
                    },
                },
            },
            props: {
                itemId: 'test-item',
                data: { 
                    item: mockStore.items[0], 
                    details: [], 
                    inventoryItems: [] 
                }, 
                itemSize: 'medium',
                currency: 'USD', 
            },
        })

        await flushPromises()
        await wrapper.vm.$nextTick()
        
        store.addItemToCart({ id: 'test-item', cost: 10 })
        mockStore.order.push({ id: 'test-item', cost: 10 }) // Update the mock store

        expect(store.order.length).toBe(3)

        const totalCost = store.order.reduce((sum, item) => sum + item.cost, 0)
        expect(totalCost).toBe(40) // 10 + 20 + 10
    })
})
