import { defineStore } from 'pinia'
import { useEotlCore } from '@eotl/core/store'

export const useStore = defineStore('@eotl/ui-ordering', {
    state: () => ({
        config: window.orderingConfig,
        order: [],
        perOrder: true,
        name: '',
        message: '',
        pickupBuddy: '',
        categories: [],
        items: [],
    }),
    getters: {
        hasRemoteInventoryUrl() {
            const { inventoryUrl } = this.config
            return inventoryUrl !== '' && !inventoryUrl.startsWith('/')
        }
    },
    actions: {
        async fetchCategories() {
            const eotlStore = useEotlCore()
            const response = await eotlStore.fetchGet('/categories')

            if (response.status === 'success') {
                this.categories.push(...response.data)
            } else {
                eotlStore.alertWarning(`Error getting the categories ${response.error.value}`)
            }
        },
        async fetchItems(category) {
            const eotlStore = useEotlCore()
            this.items.splice(0, this.items.length)

            const response = await eotlStore.fetchGet(`/items/category/${category}`)

            if (response.status === 'success') {
                this.items.push(...response.data)
            } else {
                eotlStore.alertWarning(`There was an error getting the list of items. ${response.error.value}`)
            }
        },
        addToOrder(item) {
            this.order.push(item)
        },
        updateItemQuantity(update) {
            const index = this.order.findIndex((item) => item.item === update.itemID)
            this.order[index].quantity = update.quantity
        },
        removeFromOrder(itemID) {
            const index = this.order.findIndex((item) => item.item === itemID)
            this.order.splice(index, 1)
        },
        toggleContributionType() {
            this.perOrder = !this.perOrder
        },
        updateItemContribution(data) {
            const amount = data.input || 0
            console.log('per item contribution is', amount)
            this.order.find((item) => item.item === data.itemID).contribution = amount
            this.contribution = this.order.reduce((sum, current) => {
                return sum + parseFloat(current.contribution)
            }, 0)
        },
    },
})
