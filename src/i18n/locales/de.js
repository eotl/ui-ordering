const messages = {
    welcome: 'Willkommen',
    chooseCategory: 'Wähle eine Kategorie',
    backCategories: 'Zurück zu den Kategorien',
    yourCart: 'Deine Auswahl',
    items: 'Artikel',
    estimatedPrice: 'Geschätzter Preis',
    count: 'Stück',
    unusualItem: 'Besonderer Artikel',
    btns: {
        addItem: 'In die Box',
        removeItem: 'Doch nicht',
        perItem: 'Pro Artikel',
        perOrder: 'Gesamte Bestellung',
        placeOrder: 'Jetzt bestellen',
    },
    details: {
        basePrice: 'Basispreis',
        quality: 'Qualität',
        allergens: 'Allergene',
        shops: 'Shops',
        organic: 'Bio',
    },
    sizes: {
        sm: 'Klein',
        md: 'Mittel',
        rg: 'Standard',
        lg: 'Groß',
        xl: 'Sehr groß',
        item: 'Artikel',
    },
    checkout: {
        yourOrder: 'Deine Bestellung',
        howContribute: 'Wie möchtest du beitragen?',
        yourName: 'Wer bist du?',
        note: 'Platz für eine Nachricht',
        pickupPerson: 'Wähle einen Pickup Buddy',
        noItems: 'Keine Artikel',
        noItemsInCart: 'Deine Box ist leer',
        validateName: 'Sag uns wer du bist, z.B. dein Name oder Nickname!',
        validatePickupBuddy: 'Von wem möchtest du deine Bestellung entgegennehmen? (pickup buddy)',
    },
}

export default messages
