import { createI18n } from 'vue-i18n'
import { messages } from './locales'

const i18n = createI18n({
    locale: 'en',
    legacy: false,
    messages,
    silentFallbackWarn: true,
})

export default i18n
