const Home = () => import('@/views/Home.vue')
const Category = () => import('@/views/Category.vue')
const Item = () => import('@/views/Item.vue')
const Checkout = () => import('@/views/Checkout.vue')

export const createRoutes = (pathPrefix = '/') => [
    {
        path: `${pathPrefix}`,
        name: 'ordering:home',
        component: Home,
        props: true,
    },
    {
        path: `${pathPrefix}category/:categoryId`,
        name: 'ordering:category',
        component: Category,
        props: true,
    },
    {
        path: `${pathPrefix}item/:itemId`,
        name: 'ordering:item',
        component: Item,
        props: true,
    },
    {
        path: `${pathPrefix}checkout`,
        name: 'ordering:checkout',
        component: Checkout,
        props: true,
    },
]
