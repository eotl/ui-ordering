import { createWebHistory, createRouter } from 'vue-router'
import { createRoutes } from './routes'

const router = createRouter({
    history: createWebHistory(),
    routes: createRoutes(),
})

export default router
