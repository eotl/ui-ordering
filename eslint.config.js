import pluginVue from 'eslint-plugin-vue'
import js from '@eslint/js'
import globals from 'globals'

export default [
    js.configs.recommended,
    ...pluginVue.configs['flat/recommended'],
    {
        languageOptions: {
            globals: {
                ...globals.browser,
                ...globals.node,
                ...globals.es2021,
            },
        },
        rules: {
            indent: ['warn', 4],
            'vue/no-v-html': 'off',
            'vue/multi-word-component-names': 'off',
            'no-unused-vars': ['warn', { vars: 'all', args: 'after-used', ignoreRestSiblings: false }],
            'vue/no-unused-components': [
                'warn',
                {
                    ignoreWhenBindingPresent: true,
                },
            ],
            semi: ['warn', 'never'],
            'vue/html-indent': [
                'error',
                4,
                {
                    ignores: ['VAttribute'],
                },
            ],
            'vue/max-attributes-per-line': [
                'warn',
                {
                    singleline: {
                        max: 8,
                    },
                    multiline: {
                        max: 3,
                    },
                },
            ],
            'vue/first-attribute-linebreak': 'off',
            'vue/html-self-closing': [0],
        },
    },
]

/*

commit 95ac5e1e5fc98f713b8b84888940c164615c16d8 (HEAD -> refactor-vue3)
Author: taye <dev@taye.me>
Date:   Wed Apr 24 15:05:52 2024 +0200

    Use postcss-nesting instead of sass

commit 946f53397dbefefca909d0b048407fd5f8285636
Author: taye <dev@taye.me>
Date:   Wed Apr 24 15:03:46 2024 +0200

    WIP: replace vuex with pinia; use mix of setup and options API
*/
