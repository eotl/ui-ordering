# Get the current user ID and group ID, to run the Docker containers as the host's current unprivileged user, instead of root.
UID := $(shell id -u)
GID := $(shell id -g)

CURRENT_DIR=$(patsubst %/,%,$(dir $(realpath $(firstword $(MAKEFILE_LIST)))))
ROOT_DIR=$(CURRENT_DIR)
CURRENT_USER=$(whoami)

DOCKER_NAME=ui_ordering
DOCKER_COMPOSE?=docker compose
DOCKER_EXEC_TOOLS_APP=$(DOCKER_COMPOSE) exec --user $(UID):$(GID) -it $(DOCKER_NAME) sh
DOCKER_EXEC_TOOLS_APP_ROOT=$(DOCKER_COMPOSE) exec -it $(DOCKER_NAME) sh

INSTALL_DEPS="apk add --no-cache git wget bash jq"

.PHONY: create shell dependencies build up start first-run stop restart clear


# Docker Commands 
create:
	export CURRENT_UID=$(UID) CURRENT_GID=$(GID);
	$(DOCKER_COMPOSE) up --build --no-recreate -d

shell: up
	$(DOCKER_EXEC_TOOLS_APP) 

shell-root: up
	$(DOCKER_EXEC_TOOLS_APP_ROOT)

dependencies: up
	$(DOCKER_EXEC_TOOLS_APP_ROOT) -c $(INSTALL_DEPS)

packages: up
	$(DOCKER_EXEC_TOOLS_APP) -c "yarn install --ignore-engines"

serve: up
	$(DOCKER_EXEC_TOOLS_APP) -c "yarn serve --host 0.0.0.0 --port 8084"

test: up
	$(DOCKER_EXEC_TOOLS_APP) -c "yarn coverage"

build: up
	$(DOCKER_EXEC_TOOLS_APP) -c "yarn build && npm pack"

login: up
	$(DOCKER_EXEC_TOOLS_APP) -c "npm adduser"

publish: up build
	$(DOCKER_EXEC_TOOLS_APP) -c "npm publish --access=public"

up:
	export CURRENT_UID=$(UID) CURRENT_GID=$(GID); $(DOCKER_COMPOSE) up -d


# Helper Commands
first-run: create dependencies packages serve

start: up serve

stop: $(ROOT_DIR)/docker-compose.yml
	$(DOCKER_COMPOSE) kill || true
	$(DOCKER_COMPOSE) rm --force || true

restart: stop start serve

clear: stop $(ROOT_DIR)/docker-compose.yml
	$(DOCKER_COMPOSE) down -v --remove-orphans || true
	rm -rf node_modules/
